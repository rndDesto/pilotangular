import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {
  paramData = {
    itemName:'',
    quantity:''
  }
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    const queryparam = this.route.snapshot.queryParams
    this.paramData = {
      itemName: queryparam['itemName'],
      quantity:queryparam['quantity'],
    }

  }

}
