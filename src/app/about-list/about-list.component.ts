import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-about-list',
  templateUrl: './about-list.component.html',
  styleUrls: ['./about-list.component.css']
})
export class AboutListComponent {
  constructor(private router: Router) { }
  aboutLists: any[] = [
    {
      name: 'Apel',
      cart: 0,
      stock: 15
    },
    {
      name: 'Melon',
      cart: 0,
      stock: 33
    },
    {
      name: 'Pisang',
      cart: 0,
      stock: 10
    },
    {
      name: 'Jeruk',
      cart: 0,
      stock: 20
    }
  ];


  onTambah(about: any): void {
    if (about.stock > about.cart) {
      about.cart++;
    }
  }

  onKurang(about: any): void {
    if (about.cart > 0) {
      about.cart--;
    }
  }

  onSubmit(data: any): void {
    this.router.navigate(['/service'], {
      queryParams: {
        itemName: data.name,
        quantity: data.cart
      }
    });
  }
}
