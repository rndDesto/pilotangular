import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
})
export class AboutComponent {
  counter:number=1

  onIncrement(){
    this.counter++
  }

  onDecrement(){
    if(this.counter <= 0){
      return
    }
    else{
      this.counter--
    }
  }
}
