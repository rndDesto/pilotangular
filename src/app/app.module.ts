import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { AboutListComponent } from './about-list/about-list.component';
import { ServiceComponent } from './service/service.component';
import { ProdukComponent } from './produk/produk.component';
import { ProdukDetailComponent } from './produk/produk-detail/produk-detail.component';
import { NavigasiComponent } from './navigasi/navigasi.component';
import { BlogsComponent } from './blogs/blogs.component';
import { BlogDetailComponent } from './blogs/blog-detail/blog-detail.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    AboutListComponent,
    ServiceComponent,
    ProdukComponent,
    ProdukDetailComponent,
    NavigasiComponent,
    BlogsComponent,
    BlogDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
