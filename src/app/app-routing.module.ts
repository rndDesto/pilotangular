import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { BlogsComponent } from './blogs/blogs.component';
import { ProdukComponent } from './produk/produk.component';
import { ServiceComponent } from './service/service.component';

const routes: Routes = [
  { path: '', component: AboutComponent },
  { path: 'service', component: ServiceComponent },
  { path: 'produk', component: ProdukComponent },
  { path: 'blogs', component: BlogsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
