import { Component } from '@angular/core';
interface MenuItem {
  label: string;
  route: string;
}

@Component({
  selector: 'app-navigasi',
  templateUrl: './navigasi.component.html',
  styleUrls: ['./navigasi.component.css']
})
export class NavigasiComponent {
  listMenu:MenuItem[]=[
    { label: 'Home', route: '/' },
    { label: 'Service', route: '/service' },
    { label: 'Produk', route: '/produk' },
    { label: 'Blog', route: '/blogs' }
  ]

}
