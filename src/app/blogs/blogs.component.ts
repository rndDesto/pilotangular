import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


type Game = {
  id: string;
  name: string;
  released: string;
};

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
  data?: Game[];
  page = 1;
  isLoading = false;


  constructor(private http: HttpClient) {
    console.log("environment - ", environment)
  }

  ngOnInit() {
    this.getData();
  }


  getData() {
    this.isLoading = true;
    this.http.get<{ results: Game[] }>(`https://api.rawg.io/api/games?key=36d7e80733e849a08c1a8da2c26b790a&page=${this.page}&page_size=20`)
      .subscribe(
        (response) => {
          this.data = response.results;
          this.isLoading = false;
        },
        (error) => {
          console.log('Error:', error);
          this.isLoading = false;
        }
      );
  }
  onNextPage() {
    this.page++
    this.getData();
  }
  onNextPrev() {
    if (this.page > 1) {
      this.page--
      this.getData();
    }
  }

  onDetail(detailItem: any) {
    console.log(detailItem)

  }
}