import { Component } from '@angular/core';

@Component({
  selector: 'app-produk',
  templateUrl: './produk.component.html',
  styleUrls: ['./produk.component.css']
})
export class ProdukComponent {
  nama: string = 'Data dari Parent';

  onDataUpdated(dataForm: string) {
    this.nama = dataForm;
  }
}
