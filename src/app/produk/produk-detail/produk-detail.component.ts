import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-produk-detail',
  templateUrl: './produk-detail.component.html',
  styleUrls: ['./produk-detail.component.css']
})
export class ProdukDetailComponent {
  @Input() dataParent?: string;
  @Output() dataUpdated: EventEmitter<string> = new EventEmitter<string>();

  updateData(param:any) {
    this.dataUpdated.emit(param);
  }
}
